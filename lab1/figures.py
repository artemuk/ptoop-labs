class BaseFigure(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = None

    def draw(self, ctx):
        raise NotImplementedError()


class FigureList(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.figures = []

    def add(self, figure):
        assert issubclass(type(figure), BaseFigure), 'Invalid object type'
        self.figures.append(figure)

    def draw(self, ctx):
        for x in self.figures:
            x.draw(ctx)


class Segment(BaseFigure):
    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        self.a, self.b = points

    def draw(self, ctx):
        ctx.drawLine(*self.a, *self.b)


class Rectangle(BaseFigure):
    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        self.p = a
        self.w = b[0] - a[0]
        self.h = b[1] - a[1]

    def draw(self, ctx):
        ctx.drawRect(*self.p, self.w, self.h)


class Square(Rectangle):
    def __init__(self, points, *args, **kwargs):
        super().__init__(points, *args, **kwargs)
        t = min(self.w, self.h)
        self.w, self.h = t, t


class Rhombus(BaseFigure):
    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        c = (2 * b[0] - a[0], a[1])
        d = (b[0], 2 * a[1] - b[1])
        self.segments = [(a, b), (b, c), (c, d), (d, a)]

    def draw(self, ctx):
        for a, b in self.segments:
            ctx.drawLine(*a, *b)


class Ellipse(Rectangle):
    def draw(self, ctx):
        ctx.drawEllipse(*self.p, self.w, self.h)


class Circle(Square):
    def draw(self, ctx):
        ctx.drawEllipse(*self.p, self.w, self.h)
