import sys

from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout
from .figures import FigureList, Segment, Rectangle, Square, Rhombus, Ellipse, Circle


class App(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_widget = QWidget(self)
        self._layout = QGridLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.main_widget.setLayout(self._layout)

        self.setCentralWidget(self.main_widget)
        self.setWindowTitle('Lab1')
        self.resize(700, 400)

        self.figures = FigureList()
        self.figures.add(
            Segment([(200, 100), (200, 300)])
        )
        self.figures.add(
            Square([(100, 100), (300, 300)])
        )
        self.figures.add(
            Rhombus([(100, 200), (200, 100)])
        )
        self.figures.add(
            Circle([(100, 100), (300, 300)])
        )
        self.figures.add(
            Rectangle([(400, 150), (600, 250)])
        )
        self.figures.add(
            Ellipse([(400, 150), (600, 250)])
        )

    def paintEvent(self, event):
        with QPainter(self) as painter:
            painter.setRenderHint(QPainter.Antialiasing, True)
            self.figures.draw(painter)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    ex.show()
    sys.exit(app.exec_())
