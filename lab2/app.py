import sys

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout
from PyQt5.QtWidgets import QWidget, QPushButton, QVBoxLayout

from lab2.figures import FigureList, FIGURES


class Sidebar(QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._layout = QVBoxLayout()
        self._layout.setContentsMargins(5, 5, 5, 5)
        self._layout.setAlignment(Qt.AlignTop)
        self.setLayout(self._layout)
        self.buttons = {}
        self.setMaximumWidth(100)

    def add(self, text, handler):
        self.buttons[text] = QPushButton(text)
        self.buttons[text].clicked.connect(handler)
        self._layout.addWidget(self.buttons[text])


class App(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_widget = QWidget(self)
        self._layout = QGridLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.main_widget.setLayout(self._layout)

        self.figures = FigureList()
        self.current_points = []
        self.current_figure = None

        self.sidebar = Sidebar()

        for x in FIGURES:
            def set_curr_fig(x):
                def helper():
                    self.current_points = []
                    self.current_figure = x

                return helper

            self.sidebar.add(x.NAME, set_curr_fig(x))

        self.sidebar.add('Reset', self.reset)

        self._layout.addWidget(QWidget(), 0, 0)
        self._layout.addWidget(self.sidebar, 0, 1)

        self.setCentralWidget(self.main_widget)
        self.setWindowTitle('Lab2')
        self.resize(1000, 500)

    def reset(self):
        self.figures = FigureList()
        self.current_points = []
        self.current_figure = None
        self.update()

    def paintEvent(self, event):
        with QPainter(self) as painter:
            painter.setRenderHint(QPainter.Antialiasing, True)
            self.figures.draw(painter)

    def mousePressEvent(self, event):
        if not self.current_figure:
            return

        self.current_points.append(
            (event.pos().x(), event.pos().y())
        )

        if len(self.current_points) >= self.current_figure.NUM_POINTS:
            new_fig = self.current_figure(self.current_points)
            self.figures.add(new_fig)
            self.current_points = []
            self.update()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    ex.show()
    sys.exit(app.exec_())
