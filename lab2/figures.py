from inspect import isclass

from . import drawer


class BaseFigure(object):
    NAME = None
    NUM_POINTS = -1


class FigureList(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.figures = []

    def add(self, figure):
        assert issubclass(type(figure), BaseFigure), 'Invalid object type'
        self.figures.append(figure)

    def draw(self, ctx):
        for x in self.figures:
            drawer.draw(x, ctx)


class Segment(BaseFigure):
    NAME = 'Segment'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        self.a, self.b = points


class Rectangle(BaseFigure):
    NAME = 'Rectangle'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        self.p = a
        self.w = b[0] - a[0]
        self.h = b[1] - a[1]


class Square(Rectangle):
    NAME = 'Square'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(points, *args, **kwargs)
        t = min(self.w, self.h)
        self.w, self.h = t, t


class Rhombus(BaseFigure):
    NAME = 'Rhombus'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        c = (2 * b[0] - a[0], a[1])
        d = (b[0], 2 * a[1] - b[1])
        self.segments = [(a, b), (b, c), (c, d), (d, a)]


class Ellipse(Rectangle):
    NAME = 'Ellipse'
    NUM_POINTS = 2


class Circle(Square):
    NAME = 'Circle'
    NUM_POINTS = 2


FIGURES = [
    v for k, v in globals().items()
    if isclass(v) and v is not BaseFigure and issubclass(v, BaseFigure)
]
