def draw(figure, ctx):
    try:
        draw_figure = globals()['draw_' + figure.NAME.lower()]
        draw_figure(figure, ctx)
    except KeyError:
        raise Exception(f'Unable to find draw method for {figure.NAME}')


def draw_segment(f, ctx):
    ctx.drawLine(*f.a, *f.b)


def draw_rectangle(f, ctx):
    ctx.drawRect(*f.p, f.w, f.h)


def draw_square(f, ctx):
    ctx.drawRect(*f.p, f.w, f.h)


def draw_rhombus(f, ctx):
    for a, b in f.segments:
        ctx.drawLine(*a, *b)


def draw_ellipse(f, ctx):
    ctx.drawEllipse(*f.p, f.w, f.h)


def draw_circle(f, ctx):
    ctx.drawEllipse(*f.p, f.w, f.h)

