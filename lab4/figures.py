from inspect import isclass
from glob import glob
from importlib import import_module

from . import drawer


class BaseFigure(object):
    NAME = None
    NUM_POINTS = -1


class FigureList(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.figures = []

    def add(self, figure):
        assert issubclass(type(figure), BaseFigure), 'Invalid object type'
        self.figures.append(figure)

    def draw(self, ctx):
        for x in self.figures:
            drawer = FIGURES[x.__class__]
            drawer(x, ctx)


class Segment(BaseFigure):
    NAME = 'Segment'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        self.a, self.b = points


class Rectangle(BaseFigure):
    NAME = 'Rectangle'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        self.p = a
        self.w = b[0] - a[0]
        self.h = b[1] - a[1]


class Square(Rectangle):
    NAME = 'Square'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(points, *args, **kwargs)
        t = min(self.w, self.h)
        self.w, self.h = t, t


class Rhombus(BaseFigure):
    NAME = 'Rhombus'
    NUM_POINTS = 2

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 2, 'Invalid number of points'
        a, b = sorted(points)
        c = (2 * b[0] - a[0], a[1])
        d = (b[0], 2 * a[1] - b[1])
        self.segments = [(a, b), (b, c), (c, d), (d, a)]


class Ellipse(Rectangle):
    NAME = 'Ellipse'
    NUM_POINTS = 2


class Circle(Square):
    NAME = 'Circle'
    NUM_POINTS = 2


FIGURES = {
    v: getattr(drawer, 'draw_' + v.NAME.lower()) for k, v in globals().items()
    if isclass(v) and v is not BaseFigure and issubclass(v, BaseFigure)
}

for f in glob('plugins/*.py'):
    mod_name = f'lab4.' + f.split('.')[0].replace('/', '.')
    mod = import_module(mod_name)

    try:
        export = getattr(mod, '__EXPORT__')
        FIGURES[export['figure']] = export['drawer']
        print(f""""{export['figure'].NAME}" has been loaded from {f}""")
    except AttributeError:
        pass
