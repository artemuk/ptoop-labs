from lab4.figures import BaseFigure


class Triangle(BaseFigure):
    NAME = 'Triangle'
    NUM_POINTS = 3

    def __init__(self, points, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert len(points) == 3, 'Invalid number of points'
        a, b, c = sorted(points)
        self.segments = [(a, b), (b, c), (c, a)]


def draw_triangle(f, ctx):
    for a, b in f.segments:
        ctx.drawLine(*a, *b)


__EXPORT__ = {'figure': Triangle, 'drawer': draw_triangle}
